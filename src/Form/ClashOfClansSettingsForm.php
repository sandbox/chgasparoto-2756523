<?php

/**
 * @file
 * Contains \Drupal\clashofclans\Form\clashofclansSettingsForm.
 */

namespace Drupal\clashofclans\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Configure clashofclans settings for this site.
 */
class ClashOfClansSettingsForm extends ConfigFormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'clashofclans_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return ['clashofclans.settings'];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->configFactory->get('clashofclans.settings');

        $coc_developer_url = 'http://developer.clashofclans.com';
        $url = Url::fromUri($coc_developer_url);
        $link = Link::fromTextAndUrl($coc_developer_url, $url);

        $form['text'] = array(
            '#type' => 'markup',
            '#markup' => '<p>' . t('Go to ' . $link->toString() . ' and get your Token API.') .'</p>',
        );

        $form['clashofclans_global'] = array(
            '#type' => 'fieldset',
            '#title' => t('Clash of Clans API parameters'),
        );

        $form['clashofclans_global']['clashofclans_token'] = array(
            '#type' => 'textfield',
            '#title' => t('API Token access'),
            '#default_value' => $config->get('clashofclans_token'),
        );

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $values = $form_state->getValues();
        \Drupal::configFactory()->getEditable('clashofclans.settings')
            ->set('clashofclans_token', $values['clashofclans_token'])
            ->save();

        parent::submitForm($form, $form_state);
    }

}
